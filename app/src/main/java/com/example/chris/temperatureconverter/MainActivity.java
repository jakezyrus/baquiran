package com.example.chris.temperatureconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnConvtemp;
    private TextView lblFah;
    private TextView lblCel;
    private EditText txtTempc;
    private EditText txtTempf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnConvtemp = (Button) findViewById(R.id.btnConvtemp);
        lblFah = (TextView) findViewById(R.id.lblFah);
        lblCel = (TextView) findViewById(R.id.lblCel);
        txtTempc = (EditText) findViewById(R.id.txtTempc);
        txtTempf = (EditText) findViewById(R.id.txtTempf);

        btnConvtemp.setOnClickListener(this);
    }

    public void onClick(View v) {
        if (txtTempc.getText().length() > 0) {
            float Fah = Float.parseFloat(txtTempc.getText().toString());
            float Cel = ((Fah - 32) * 5) / 9;
            lblFah.setText(Float.toString(Cel) + " F");
            lblCel.setText(txtTempc.getText().toString() + " C");
            txtTempc.setText("");
        } else {
            float Cel1 = Float.parseFloat(txtTempf.getText().toString());
            float Fah1 = ((Cel1 * 9) / 5) + 32;
            lblCel.setText(Float.toString(Fah1) + " C");
            lblFah.setText(txtTempf.getText().toString() + " F");
            txtTempf.setText("");
        }
    }
}
